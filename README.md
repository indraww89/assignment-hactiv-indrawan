
# Dokumentasi Assignment

## Fitur APPS
### Profile 
- Profile	-
digunakan untuk menampilkan detail profile pengguna aplikasi, seperti:
	- Nama lengkap
	- email
	- no HP
	- Alamat
- Order History	-
digunakan untuk melihat detail order yang telah dilakukan.

### Help
- How to Order	-
	berisi panduan lengkap mengenai cara pengguna untuk melakukan pembelian
- Payment	-
	berisi panduan lengkap mengenai cara pengguna untuk melakukan pembayaran
- Shipping	-
	berisi informasi lengkap mengenai pengiriman barang pesanan
- Profile	-
	berisi informasi lengkap mengenai detail perusahaan
- Contact Us	-
	berisi informasi lengkap mengenai kontak perusahaan

### Category
Pada halaman ini, pengguna dapat memfilter barang yang akan ditampilkan berdasarkan jenis barangnya, misalnya:
- Electronic
- Fashion
- Baby Gear
- Home & Furniture
- Health & Sport
- Office & Indostry

### Recent
Pada halaman ini, aplikasi akan menampilkan seluruh barang yang dijual, lengkap dengan gambar, judul, dan harga barangnya.

### Keranjang
Menampilkan barang-barang yang telah dipilih untuk ditransaksikan.

### Search
Digunakan untuk memfilter barang berdasarkan nama barangnya.


## Test Case
### Test Case
- `ECOM_001` - Test case untuk menjalankan test edit profile
- `ECOM_002` - Test case untuk menjalankan cek order history
- `ECOM_003` - Test case untuk menjalankan test halaman help, meliputi how to order, payment, dan shipping
- `ECOM_004` - Test case untuk menjalankan test halaman help, meliputi profile dan contact us
- `ECOM_005` - Test case untuk menjalankan test untuk menampilkan produk berdasarkan kategory yang dipilih
- `ECOM_006` - Test case untuk menjalankan test untuk menampilkan produk berdasarkan kata kunci dari pencarian
- `ECOM_007` - Test case untuk menjalankan test untuk melakukan checkout order

### Data Files
- `Profile`	- Baris data yang digunakan saat melakukan edit data.
- `Order`		- Baris data yang digunakan saat melakukan checkout order

### Active Data
`Document`
- Sheet Prof	- digunakan untuk data files Profile
- Sheet Item_order	- digunakan untuk data files Order

### APK
`Ecommerce-SAMPLE-Android.apk`


